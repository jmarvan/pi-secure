package com.synapticpath.raspberry;

import org.junit.Ignore;
import org.junit.Test;

import com.synapticpath.pisecure.model.SecurityEvent;
import com.synapticpath.pisecure.model.SecurityEvent.Severity;
import com.synapticpath.pisecure.model.SystemEvent;
import com.synapticpath.pisecure.model.SystemEvent.Type;
import com.synapticpath.pisecure.modules.HttpSMSNotificationModule;

public class HttpSMSNotificationModuleTest extends AbstractTest {
	
	/**
	 * This test creates an event and uses the {@link HttpSMSNotificationModule} to send it through SMS gateway.
	 * 
	 * Note that by default the config.properties located in the test/resources folder do not contain sms gateway configuration.
	 * Refer to the config.properties in main/resources for sample configuration.
	 * 
	 * @throws Exception
	 */
	@Ignore
	@Test
	public void testSms() throws Exception {

		HttpSMSNotificationModule sender = new HttpSMSNotificationModule();
		sender.configure(config);
		
		SystemEvent event = SecurityEvent.create(Type.SETSTATE, Severity.HIGH, "test");
		sender.onEvent(event);
	}

}
