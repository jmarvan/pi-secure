/* Copyright (C) 2016 synapticpath.com - All Rights Reserved

 This file is part of Pi-Secure.

    Pi-Secure is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pi-Secure is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pi-Secure.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.synapticpath.raspberry;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import com.synapticpath.pisecure.LoginService;
import com.synapticpath.pisecure.SecuritySystem.SystemState;
import com.synapticpath.pisecure.model.SystemEvent;
import com.synapticpath.pisecure.model.SystemEvent.Type;
import com.synapticpath.pisecure.modules.LoginModule;

/**
 * Unit test for simple App.
 */
public class AppTest extends AbstractTest {
	
	@Test
	public void testLoginService() throws Exception {
				
		LoginService li = new LoginModule();
		((LoginModule)li).configure(config);
		
		loginFail(li, "1");
		loginFail(li,"2");
		loginFail(li,"3");
		loginFail(li,"1234");  //Even though correct pin supplied, login still fails as lock occurs
		Thread.sleep(1050);
		
        
		loginFail(li,"5");
		loginFail(li,"6");
		loginFail(li,"7");
		loginFail(li, "1234");
        
        Thread.sleep(1050);
        
        loginFail(li,"9");
        loginFail(li,"10");
        loginSuccess(li, "1234");  

        loginFail(li,"11");
        loginFail(li,"12");
        loginFail(li,"13");
        loginFail(li, "1234");
        
	}
	
	private void loginFail(LoginService li, String text) {
		
		String token = li.login(text);
		assertNull(token);
		
	}
	
	private String loginSuccess(LoginService li, String text) {
		
		String token = li.login(text);
		assertNotNull(token);
		
		return token;
		
	}
	
	@Test
	public void testDelayedArm() throws Exception {
		//Attempting to arm system, 
		config.getSystemModule().accept(SystemEvent.create(Type.SETSTATE, "test", SystemState.DELAYED_ARM).delay(200L));
		assertEquals(SystemState.DELAYED_ARM, config.getSystemModule().getState());
		Thread.sleep(100);
		//During delayed arm, there nothing should cause alarm until state becomes ARMED, configuration is 500ms.
		config.getSystemModule().accept(SystemEvent.create(Type.SENSOR, "test"));
		config.getSystemModule().accept(SystemEvent.create(Type.TAMPER, "test"));
		
		assertEquals(SystemState.DELAYED_ARM, config.getSystemModule().getState());
		Thread.sleep(150);
		
		//At this point the system should be in armed mode.
		assertEquals(SystemState.ARMED, config.getSystemModule().getState());
		
		config.getSystemModule().accept(SystemEvent.create(Type.SETSTATE, "test", SystemState.DISARMED));
		
		config.getSystemModule().accept(SystemEvent.create(Type.SETSTATE, "test", SystemState.DELAYED_ARM).delay(200L));
		assertEquals(SystemState.DELAYED_ARM, config.getSystemModule().getState());
		
		//Wait about 1/2 the time, then disarm.
		Thread.sleep(100);
		config.getSystemModule().accept(SystemEvent.create(Type.SETSTATE, "test", SystemState.DISARMED));
		assertEquals(SystemState.DISARMED, config.getSystemModule().getState());
		
		Thread.sleep(150);
		//Make sure that system still disarmed after initial arm timeout.
		assertEquals(SystemState.DISARMED, config.getSystemModule().getState());
		
	}
	
	@Test
	public void testAlertMode() throws Exception {
		setAndTestArmed();
		
		config.getSystemModule().accept(SystemEvent.create(Type.SENSOR, "test"));
		assertEquals(SystemState.ALERTED, config.getSystemModule().getState());
		
		Thread.sleep(150); //After 150ms system should be back to armed.
		assertEquals(SystemState.ARMED, config.getSystemModule().getState());
		
		config.getSystemModule().accept(SystemEvent.create(Type.SENSOR, "test").delay(150L));  //delay doesn't matter
		//Again in alert mode
		assertEquals(SystemState.ALERTED, config.getSystemModule().getState());
		
		//but another event under 100ms.
		Thread.sleep(50);
		config.getSystemModule().accept(SystemEvent.create(Type.SENSOR, "test"));
		assertEquals(SystemState.ALARM, config.getSystemModule().getState());
	}
	
	@Test
	public void testDelayedAlarm() throws Exception {
		setAndTestArmed();
		
		//Another 
		config.getSystemModule().accept(SystemEvent.create(Type.SENSOR, "test").delay(150L));
		assertEquals(SystemState.ALERTED, config.getSystemModule().getState());		
		
		config.getSystemModule().accept(SystemEvent.create(Type.SENSOR, "test").delay(150L));		
		assertEquals(SystemState.DELAYED_ALARM, config.getSystemModule().getState());
		
		Thread.sleep(200);
		assertEquals(SystemState.ALARM, config.getSystemModule().getState());
	}
	
	@Test
	public void testTamperAlarm() throws Exception {
		
		setAndTestArmed();
		//First we'll test that ALARM occurs when in armed state
		config.getSystemModule().accept(SystemEvent.create(Type.TAMPER, "tampertest"));
		assertEquals(SystemState.ALARM, config.getSystemModule().getState());
		
		
		setAndTestArmed();
		//Standard sensor event causes alert mode
		config.getSystemModule().accept(SystemEvent.create(Type.SENSOR, "test"));
		assertEquals(SystemState.ALERTED, config.getSystemModule().getState());		
				
		config.getSystemModule().accept(SystemEvent.create(Type.TAMPER, "tampertest"));
		assertEquals(SystemState.ALARM, config.getSystemModule().getState());
		
		setAndTestArmed();
		//Standard sensor event causes alert mode
		config.getSystemModule().accept(SystemEvent.create(Type.SENSOR, "test"));
		assertEquals(SystemState.ALERTED, config.getSystemModule().getState());		
		
		//This event will cause system to go into delayed alarm
		config.getSystemModule().accept(SystemEvent.create(Type.SENSOR, "test").delay(100L));
		assertEquals(SystemState.DELAYED_ALARM, config.getSystemModule().getState());
				
		//Regardless of system state above, we will have alarm in case of TAMPER
		config.getSystemModule().accept(SystemEvent.create(Type.TAMPER, "tampertest"));
		assertEquals(SystemState.ALARM, config.getSystemModule().getState());
	}
	
	
	
	@Test
	public void testAlarm() throws Exception {
		
		setAndTestArmed();
		
		config.getSystemModule().accept(SystemEvent.create(Type.SENSOR, "test"));
		assertEquals(SystemState.ALERTED, config.getSystemModule().getState());		
				
		config.getSystemModule().accept(SystemEvent.create(Type.SENSOR, "test"));
		assertEquals(SystemState.ALARM, config.getSystemModule().getState());
	}

	
	private void setAndTestArmed() throws Exception {
		config.getSystemModule().accept(SystemEvent.create(Type.SETSTATE, "test", SystemState.ARMED));		
		//At this point the system should be in armed mode.
		assertEquals(SystemState.ARMED, config.getSystemModule().getState());
	}	

}
