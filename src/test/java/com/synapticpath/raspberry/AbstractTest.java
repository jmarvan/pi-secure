package com.synapticpath.raspberry;

import org.junit.Before;

import com.synapticpath.pisecure.Config;
import com.synapticpath.pisecure.SecuritySystemImpl;
import com.synapticpath.pisecure.modules.SimpleEventLoggerModule;
import com.synapticpath.utils.Logging;

public class AbstractTest {
	
	protected Config config;
	
	@Before
	public void init() throws Exception {
		config = new Config();
		config.init(null);
		
		SecuritySystemImpl system = new SecuritySystemImpl();
		system.configure(config);
		config.setSystemModule(system);
		
		SimpleEventLoggerModule logger = new SimpleEventLoggerModule();
		logger.configure(config);
				
		config.addModule(logger);
		Logging.setLogLevelThreshold(Logging.LogLevel.DEBUG);
	}

}
