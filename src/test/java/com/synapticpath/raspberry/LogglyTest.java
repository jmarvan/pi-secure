/* Copyright (C) 2016 synapticpath.com - All Rights Reserved

 This file is part of Pi-Secure.

    Pi-Secure is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pi-Secure is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pi-Secure.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.synapticpath.raspberry;

import org.junit.Ignore;
import org.junit.Test;

import com.synapticpath.pisecure.SecuritySystem.SystemState;
import com.synapticpath.pisecure.model.SystemEvent;
import com.synapticpath.pisecure.model.SystemEvent.Type;
import com.synapticpath.pisecure.modules.LogglyEventLoggerModule;

/**
 * Tests loggly.com logging
 * 
 * @author jmarvan@synapticpath.com
 *
 */
public class LogglyTest extends AbstractTest {
	
	
	/**
	 * Tests sending an event using {@link LogglyEventLoggerModule}.
	 * 
	 * By default this test is ignored since Loggly is a commercial service, you need an account to obtain a token to able to send logs there.  
	 * 
	 * See /main/resources/config.properties for example configuration.
	 * 
	 * 
	 * @throws Exception
	 */
    @Ignore
	@Test
	public void test() throws Exception {		
		
		LogglyEventLoggerModule logger = new LogglyEventLoggerModule();
		logger.configure(config);
		
		SystemEvent event = SystemEvent.create(Type.SENSOR, "test", SystemState.DISARMED);
		logger.onEvent(event);
		
		//Verification that event posted ok needs to be done manually by logging in to loggly.com
		
	}

}
