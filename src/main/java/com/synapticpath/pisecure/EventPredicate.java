/* Copyright (C) 2016 synapticpath.com - All Rights Reserved

 This file is part of Pi-Secure.

    Pi-Secure is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pi-Secure is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pi-Secure.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.synapticpath.pisecure;

import java.util.Set;
import java.util.function.BiPredicate;

import com.synapticpath.pisecure.SecuritySystem.SystemState;
import com.synapticpath.pisecure.model.SystemEvent;
import com.synapticpath.utils.Logging;

/**
 * An enumeration of BiPredicate that are used for event filtering or event
 * detection. 
 * 
 * @author jmarvan@synapticpath.com
 *
 */
public enum EventPredicate implements BiPredicate<SystemEvent, SystemState> {

	INACTIVE_SENSOR_EVENT((SystemEvent event, SystemState systemState) -> event.getType().isSensorEvent() && systemState.isDelayedArm()),
	INACTIVE_TAMPER_EVENT((SystemEvent event, SystemState systemState) -> event.getType().isTamperEvent() && systemState.isDelayedArm()),
	TAMPER_EVENT_ALARM_TRIGGER((SystemEvent event, SystemState systemState) -> event.getType().isTamperEvent() && !systemState.isInactive() && !systemState.isAlarm()),
	DELAYED_ARM_SETSTATE_EVENT((SystemEvent event, SystemState systemState) -> event.getType().isSetstateEvent() && event.getState().isDelayedArm()),
	DISARMED_SETSTATE_EVENT((SystemEvent event, SystemState systemState) -> event.getType().isSetstateEvent() && event.getState().isDisarmed()),
	STARTUP_EVENT((SystemEvent event, SystemState systemState) -> event.getType().isStartupEvent()),
	SHUTDOWN_EVENT((SystemEvent event, SystemState systemState) -> event.getType().isShutdownEvent());

	private BiPredicate<SystemEvent, SystemState> pr;

	private EventPredicate(BiPredicate<SystemEvent, SystemState> pr) {
		this.pr = pr;
	}

	@Override
	public boolean test(SystemEvent event, SystemState systemState) {
		return this.pr.test(event, systemState);
	}
	
	public static boolean test(SystemEvent event, Config config, EventPredicate ... predicates) {
		for (EventPredicate predicate: predicates) {
			if (predicate.test(event, config.getSystemModule().getState())) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean test(SystemEvent event, Config config, Set<EventPredicate> predicates) {
		EventPredicate [] arrPredicates = new EventPredicate [predicates.size()];
		return test(event, config, predicates.toArray(arrPredicates));
	}

}
