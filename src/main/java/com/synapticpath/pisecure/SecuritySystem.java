/* Copyright (C) 2016 synapticpath.com - All Rights Reserved

 This file is part of Pi-Secure.

    Pi-Secure is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pi-Secure is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pi-Secure.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.synapticpath.pisecure;

import java.util.function.Consumer;

import com.synapticpath.pisecure.model.SystemEvent;

/**
 * SecuritySystem interface defines public methods to which various system
 * Modules report events.
 * SecuritySystem implementation is responsible for evaluating the events
 * and will propagate them to the remainder of the system.
 * 
 *   DELAYED_ARM - This is a state that exists for some time before system goes to ARMED mode.
 *   ARMED - In this state, the system is ready and waiting for sensor events
 *   ALERTED - System is in heightened state, after certain time system goes back to ARMED state, or DELAYED_ALARM/ALARM.
 *   DELAYED_ALARM - System is in a count-down before alarm, during this time a user can disarm the system.
 *   ALARM
 *   DISARMED - This is the off state of 
 * 
 * @author jmarvan@synapticpath.com
 *
 */
public interface SecuritySystem extends Consumer<SystemEvent> {
	
	public static enum SystemState {
		DELAYED_ARM, ARMED, ALERTED, DELAYED_ALARM, ALARM, DISARMED;
		
		public boolean isActive() {
			return this.equals(ARMED) || this.equals(ALARM) || this.equals(DELAYED_ALARM) || this.equals(ALERTED);
		}
		
		public boolean isInactive() {
			return this.equals(DISARMED) || this.equals(DELAYED_ARM);
		}

		public boolean isArmed() {
			return this.equals(ARMED);
		}
		
		public boolean isDelayedArm() {
			return this.equals(DELAYED_ARM);
		}
		
		public boolean isDelayedAlarm() {
			return this.equals(DELAYED_ALARM);
		}
		
		public boolean isAlarm() {
			return this.equals(ALARM);
		}
		
		public boolean isDisarmed() {
			return this.equals(DISARMED);
		}
		
		public boolean isAlerted() {
			return this.equals(ALERTED);
		}

	}
	
	public SystemState getState(); 

}
