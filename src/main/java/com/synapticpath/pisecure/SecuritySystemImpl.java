/* Copyright (C) 2016 synapticpath.com - All Rights Reserved

 This file is part of Pi-Secure.

    Pi-Secure is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pi-Secure is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pi-Secure.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.synapticpath.pisecure;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Predicate;

import com.synapticpath.pisecure.model.SecurityEvent;
import com.synapticpath.pisecure.model.SecurityEvent.Severity;
import com.synapticpath.pisecure.model.SystemEvent;
import com.synapticpath.pisecure.model.SystemEvent.Type;
import com.synapticpath.pisecure.modules.SMTPNotificationModule;
import com.synapticpath.utils.Logging;
import static com.synapticpath.pisecure.EventPredicate.TAMPER_EVENT_ALARM_TRIGGER;

/**
 * This is the brain of the Pi-Secure security system.  It evaluates events sent
 * to it and acts on them to update its internal state, and notifies EventListeners of notable
 * changes.
 * 
 * @author jmarvan@synapticpath.com
 *
 */
public class SecuritySystemImpl implements SecuritySystem, Configurable {
	
	private SystemState state;
	
	private Queue<SystemEvent> queue = new ConcurrentLinkedQueue<SystemEvent>();
	
	private Config config;
	
	private int alarmResetDelay; //Time in milliseconds before system returns from heightened state.
	
	private int alertDelay; //Time in milliseconds before system returns from heightened state.
	
	private DelayedEventRunner alarmResetRunner;
	
	private DelayedEventRunner alertRunner;
	
	private DelayedEventRunner armDelayRunner;
	
	private DelayedEventRunner delayedAlarmRunner;
	
	private Set<EventPredicate> eventFilters;

	public SecuritySystemImpl() {	
		state = SystemState.DISARMED;	
	}
	
	public void configure(Config config) {
		this.eventFilters = new HashSet<>();
		this.config = config;		
		this.alertDelay = Integer.parseInt(config.getProperty("pisecure.alert.delay.millis", "10000"));
		this.alarmResetDelay = Integer.parseInt(config.getProperty("pisecure.alarm.reset.delay.millis", "0"));
		
		List<String> eventNames = config.getPropertyList("pisecure.event.filter.");
		eventNames.forEach(filterName -> eventFilters.add(EventPredicate.valueOf(filterName)));
	}

	public SystemState getState() {
		return state;
	}
	
	public void accept(SystemEvent event) {
				
		if (event.getState() == null) {
			event.setState(state);
		}		
		
		queue.add(event);
		Logging.trace(this, "Incoming event added to queue. %s", event.toJson());
		process();
				
	}

	protected synchronized void process() {
		
		SystemEvent event = null;
		while ((event = queue.poll()) != null) {
			
			boolean filterByPredicate = EventPredicate.test(event, config, eventFilters);
			if (!filterByPredicate && onEvent(event)) {

				Logging.trace(this, "Processing listeners for event. %s", event.toJson());
				for (EventListener listener : config.getModules(EventListener.class)) {
					listener.onEvent(event);
				}
				
			} else {
				Logging.trace(this, "Event was filtered. %s", event.toJson());
			}
		}
				
	}
		
	/**
	 * When true returned, event will not be sent to listeners.
	 * @param event
	 * @return
	 */
	protected boolean onEvent(SystemEvent event) {	
	
		Logging.debug(this, "Received event %s", event.toJson());
		switch (event.getType()) {
			case SENSOR:
				sensorEvent(event);				
				break;
			case TAMPER:				
				tamperEvent(event);
				break;
			case SETSTATE :
				setStateFromEvent(event);
				break;
				
			default: break;
		}
		
		return true;
	}

	/**
	 * Specific handling for a tamper event.
	 * Currently the event is handled only when system armed, or in any other 'higher' state of readiness.
	 * 
	 * @param event
	 */
	public void tamperEvent(SystemEvent event) {
		//TODO We can do so much more with tamper event other than causing immediate alarm.
		//For example tamper event may be significant even when system is inactive. We can send notifications, save the fact that system has been tampered with, etc.
		if (TAMPER_EVENT_ALARM_TRIGGER.test(event, getState())) {
			Logging.info(this, "TAMPER ALARM!");
			queue.add(SecurityEvent.create(Type.SETSTATE, Severity.HIGH, event.getSource()).state(SystemState.ALARM));			
		}
	}

	/**
	 * Specific handling of PIR sensor event. Typically sensor causes ALARM immediately, or if so configured a DELAYED_ALARM giving
	 * the user some time to disarm the system. Some extra logic is needed to deal with false alarms though. 
	 * 
	 * The PIR sensors used sometimes just PING once causing false alarm. This issue is solved by
	 * going into alert. Operator is notified by LOW severity channels - {@link SMTPNotificationModule} for example, so that situation
	 * can be monitored.
	 * 
	 * After some time of inactivity, the system goes back to standard {@link SystemState#ARMED} state. Further sensor activity pushes
	 * the system into full {@link SystemState#ALARM} state.
	 * 
	 * This approach however does not work well when false alarm has been triggered during a thunderstorm when it was observed that multiple
	 * sensors can go off at once. 
	 * TODO further handling of thunderstorm scenario may be needed - perhaps by moving some logic to the sensor 'driver'?
	 * 
	 * 
	 * @param event
	 */
	public void sensorEvent(SystemEvent event) {
		
		SystemEvent secEvent = null;
		
		if (state.isArmed()) {
			Logging.info(this, "SENSOR ALERT!");
			//Going into alert essentially provides a single event buffer before alarm is invoked.
			secEvent = SecurityEvent.create(Type.SETSTATE, Severity.LOW, event.getSource()).state(SystemState.ALERTED);
		}
		
		//From ALERTED state we enter either DELAYED_ALARM
		if (state.isAlerted() && event.getDelay() > 0) {
			Logging.info(this, "DELAYED SENSOR ALARM!");
			secEvent = SecurityEvent.create(Type.SETSTATE, Severity.LOW, event.getSource()).state(SystemState.DELAYED_ALARM).delay(event.getDelay());
		}
		
		//... or full blown ALARM
		if (state.isAlerted() && event.getDelay() == 0) {			
			Logging.info(this, "SENSOR ALARM!");
			secEvent = SecurityEvent.create(Type.SETSTATE, Severity.HIGH, event.getSource()).state(SystemState.ALARM);			
		}
		
		if (secEvent != null) {
			queue.add(secEvent);
		}
		//We must be in DELAYED_ALARM or ALARM and another sensor triggered - this changes nothing
	}
	
	private void setStateFromEvent(SystemEvent event) {
		
		Logging.info(this, "Setting system state to %s", event.getState());
		this.state = event.getState();
		//Consider event to take other actions.
		checkAlarm(event);
		checkDelayedArm(event);
		checkDelayedAlarm(event);
		checkAlert(event);
	}
	
	private void checkAlarm(SystemEvent event) {		
		
		if (event.getState().isAlarm() && alarmResetDelay > 0) {
			SystemEvent alertEvent = SecurityEvent.create(Type.SETSTATE, Severity.LOW, "system").state(SystemState.ARMED);		
			alarmResetRunner = new DelayedEventRunner(alertEvent, (state) -> state.isAlarm() , alarmResetDelay);
			fireDelayedEvent(alarmResetRunner);
		}
	}
	
	private void checkAlert(SystemEvent event) {
				
		if (event.getState().isAlerted()) {
			SystemEvent alertEvent = SystemEvent.create(Type.SETSTATE, "system").state(SystemState.ARMED);		
			alertRunner = new DelayedEventRunner(alertEvent, (state) -> state.isAlerted() ,alertDelay);
			fireDelayedEvent(alertRunner);
		}
	}
	
	private void checkDelayedAlarm(SystemEvent event) {
		
		if (event.getState().isDelayedAlarm()) {
			SystemEvent delayedEvent = SecurityEvent.create(Type.SETSTATE, Severity.HIGH, "system").state(SystemState.ALARM);			
			//The only thing that can stop delayed alarm is disabling of the system
			delayedAlarmRunner = new DelayedEventRunner(delayedEvent, (state) -> !state.isDisarmed(), event.getDelay());
			fireDelayedEvent(delayedAlarmRunner);
		}
	}
	
	private void checkDelayedArm(SystemEvent event) {		
		
		if (event.getState().isDelayedArm()) {
			SystemEvent delayedEvent = SystemEvent.create(Type.SETSTATE, "system").state(SystemState.ARMED);
			armDelayRunner = new DelayedEventRunner(delayedEvent, (state) -> state.isDelayedArm(), event.getDelay());
			fireDelayedEvent(armDelayRunner);			
		}		
	}
	
	private void fireDelayedEvent(DelayedEventRunner runner) {
		if (runner.getDelay() > 0) {
			new Thread(runner).start();
		} else {
			runner.fireEvent();
		}
	}
	
	
	private class DelayedEventRunner implements Runnable {
	
		private Predicate<SystemState> acceptPredicate;
		private long delay;
		private SystemEvent eventToSend;
		
		public DelayedEventRunner(SystemEvent eventToSend, long delay) {
			this.delay = delay;
			this.eventToSend = eventToSend;
		}
		
		public DelayedEventRunner(SystemEvent eventToSend, Predicate<SystemState> acceptPredicate, long delay) {
			this.delay = delay;
			this.eventToSend = eventToSend;
			this.acceptPredicate = acceptPredicate;
		}
		
		public long getDelay() {
			return delay;
		}

		public void cancel() {
			this.acceptPredicate = (state) -> false;
		}				

		@Override
		public void run() {
			try {
				Thread.sleep(delay);
				fireEvent();
				
			}catch (InterruptedException ie) {
				
			}
		}
		
		public void fireEvent() {
			if (acceptPredicate == null || acceptPredicate.test(getState())) {
				
				Logging.info(this, "Firnig delayed event %s", eventToSend.toJson());
				eventToSend.setTime(new Date());
				accept(eventToSend);
			}
		}
		
	}

}
