/* Copyright (C) 2016 synapticpath.com - All Rights Reserved

 This file is part of Pi-Secure.

    Pi-Secure is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pi-Secure is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pi-Secure.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.synapticpath.pisecure.modules;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import com.synapticpath.pisecure.Config;
import com.synapticpath.pisecure.Configurable;
import com.synapticpath.pisecure.Disableable;
import com.synapticpath.pisecure.EventListener;
import com.synapticpath.pisecure.Module;
import com.synapticpath.pisecure.model.SystemEvent;
import com.synapticpath.utils.Logging;

/**
 * This module is responsible for sending incoming events to Loggly logging service.
 * 
 * Specifically this is using API documented here https://www.loggly.com/docs/http-endpoint/
 * 
 * @author jmarvan@synapticpath.com
 *
 */
@Module
public class LogglyEventLoggerModule implements Disableable, EventListener, Configurable {
	
	private String postRequestUrl;	
	
	private String tag;
	
	private String token;
	
	private boolean disabled;
	
	

	@Override
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;		
	}

	@Override
	public boolean isDisabled() {
		return disabled;
	}

	@Override
	public void configure(Config config) throws Exception {
		postRequestUrl = config.getProperty("loggly.logger.post.url", true);
		
		disabled = Boolean.valueOf(config.getProperty("loggly.logger.disabled", "true"));
		
		tag = config.getProperty("loggly.tag", true);
		token = config.getProperty("loggly.token", true);		
			
	}

	@Override
	public void onEvent(SystemEvent event) {		
		if (disabled) {
			return;
		}
		logEvent(event);
	}

	
	private void logEvent(SystemEvent event) {
		
		try {
			byte[] postData = event.toJson().getBytes(StandardCharsets.UTF_8);
	
			int postDataLength = postData.length;
			URL url = new URL(String.format(postRequestUrl, token, tag));
	
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setInstanceFollowRedirects(false);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			conn.setRequestProperty("charset", "utf-8");
			conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
			conn.setUseCaches(false);
			try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
				wr.write(postData);
			}
			
			if (conn.getResponseCode() == 200) {
				BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				StringBuilder response = new StringBuilder();
				String inputLine = null;
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();				
			}
			
		} catch (Exception e) {
			Logging.error(this, "Error sending event to loggly.", e);
		}
	}
}
